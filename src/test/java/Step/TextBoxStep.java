package Step;

import Page.TextBoxPage;
import Support.DriverInit;
import io.cucumber.java.After;
import io.cucumber.java.pt.*;
import org.openqa.selenium.WebDriver;

public class TextBoxStep {

    private WebDriver driver;
    DriverInit getDriver = new DriverInit(driver);
    TextBoxPage tbPage;

    @Dado("que tenho a página {string}")
    public void que_tenho_a_página(String href) {
        tbPage = getDriver.getDriver(href);
    }

    @Quando("digitar nos campos {string}, {string}, {string}, {string}, {string}, {string}, {string} e {string}")
    public void digitar_nos_campos_e(String nome, String ultimoNome, String email, String endereco, String universidade, String profissao, String genero, String idade) {
        tbPage.preencherCampos(nome, ultimoNome, email, endereco, universidade, profissao, genero, idade);
    }

    @Quando("clicar em CRIAR")
    public void clicar_em_criar() {
        tbPage.clicarEmCriar();
    }

    @Então("deve salvar o cadastro com sucesso conferindo a mensagem {string}")
    public void deve_salvar_o_cadastro_com_sucesso_conferindo_a_mensagem(String mensagem) {
        tbPage.validaInsercao(mensagem);
    }

    @After
    public void fechaNavegador(){
        getDriver.closeDriver();
    }
}
