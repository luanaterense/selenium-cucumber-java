package Support;

import Page.TextBoxPage;
import Step.TextBoxStep;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverInit {

    private WebDriver driver;

    public DriverInit(WebDriver driver){
        this.driver = driver;
    }

    public TextBoxPage getDriver(String href) {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/driver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://automacaocombatista.herokuapp.com" + href);
        return new TextBoxPage(driver);
    }

    public TextBoxPage closeDriver() {
        driver.quit();
        return new TextBoxPage(driver);
    }

}

