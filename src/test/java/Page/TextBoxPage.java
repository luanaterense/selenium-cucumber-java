package Page;

import Support.DriverInit;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TextBoxPage {

    static WebDriver driver;

    public TextBoxPage(WebDriver driver){
        this.driver = driver;
    }

    public void preencherCampos(String nome, String ultimoNome, String email, String endereco,
                                String universidade, String profissao, String genero, String idade){

        try {
            WebElement name = driver.findElement(By.name("user[name]"));

            WebDriverWait wait = new WebDriverWait(driver, 2000);

            wait.until(ExpectedConditions.visibilityOf(name));
            name.sendKeys(nome);
            driver.findElement(By.name("user[lastname]")).sendKeys(ultimoNome);
            driver.findElement(By.name("user[email]")).sendKeys(email);
            driver.findElement(By.name("user[address]")).sendKeys(endereco);
            driver.findElement(By.name("user[university]")).sendKeys(universidade);
            driver.findElement(By.name("user[profile]")).sendKeys(profissao);
            driver.findElement(By.name("user[gender]")).sendKeys(genero);
            driver.findElement(By.name("user[age]")).sendKeys(idade);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public void clicarEmCriar(){
        driver.findElement(By.name("commit")).click();
    }

    public void validaInsercao(String mensagem){
        try{
            String lblMsg = driver.findElement(By.xpath("//div[contains(text(),'Usuário Criado com sucesso')]")).getText();
            Assert.assertEquals(mensagem, lblMsg);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
