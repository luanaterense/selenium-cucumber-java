#language: pt

Funcionalidade:
  Como estudante
  Quero realizar testes de componentes
  Para aprender cada dia mais

  Esquema do Cenário:
    Dado que tenho a página "<href>"
    Quando digitar nos campos "<nome>", "<ultimoNome>", "<email>", "<endereco>", "<universidade>", "<profissao>", "<genero>" e "<idade>"
    E clicar em CRIAR
    Então deve salvar o cadastro com sucesso conferindo a mensagem "<usuarioSalvoComSucesso>"

    Exemplos:
      | href       | nome  | ultimoNome | email              | endereco | universidade | profissao | genero | idade |usuarioSalvoComSucesso     |
      | /users/new | Maria | Silva      | maria@teste.com.br | Rua Um   | FAC          | QA        | F      | 29    |Usuário Criado com sucesso |
